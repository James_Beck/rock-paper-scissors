﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPS
{
    class Images
    {
        public static List<ModelClass> imagesListGames()
        {
            List<ModelClass> gameimages = new List<ModelClass>
            {
                new ModelClass { image1 = "http://www.iconninja.com/files/23/72/877/rock-icon.png", image2 = "http://findicons.com/files/icons/85/kids/128/editcopy.png", image3 = "https://image.flaticon.com/icons/png/128/124/124818.png"},
                new ModelClass { image1 = "https://cdn3.iconfinder.com/data/icons/nature-animals/512/Bird-128.png", image2 = "http://www.iconninja.com/files/367/228/892/armadillo-icon.png", image3 = "http://icons.iconarchive.com/icons/iconsmind/outline/128/Fish-icon.png"},
                new ModelClass { image1 = "http://icons.iconarchive.com/icons/aha-soft/free-large-boss/128/Superman-icon.png", image2 = "http://s23.postimg.org/ty0r56uuf/Kryptonite_Crystal.png", image3 = "http://static.memecdn.com/images/avatars/s_1060111_524947aba94a7.jpg"},
                new ModelClass { image1 = "http://zoywiki.com/images/9/94/LotS_Nanobot_Generator.png", image2 = "http://vignette1.wikia.nocookie.net/callofduty/images/9/91/EMP_Grenade_Menu_Icon_BOII.png/revision/latest?cb=20121210121913", image3 = "http://findicons.com/files/icons/989/ivista_2/128/user.png"},
                new ModelClass { image1 = "https://cdn3.iconfinder.com/data/icons/iconka-buddy-set/128/angel_128.png", image2 = "http://www.heroesfire.com/images/wikibase/icon/talents/demonic-form.png", image3 = "http://findicons.com/files/icons/989/ivista_2/128/user.png"},
                new ModelClass { image1 = "https://68.media.tumblr.com/avatar_ec247d9e798d_128.png", image2 = "http://img1.yiwugou.com/i004/2016/09/04/95/a7b8cbf3209ee44370562ef9cc48294b.jpg@128w_128h.jpg", image3 = "https://68.media.tumblr.com/avatar_40ea6a3758e9_128.png"},
                new ModelClass { image1 = "http://www.i2clipart.com/cliparts/8/f/4/8/clipart-titanic-8f48.png", image2 = "http://static.greatbigcanvas.com/images/square/alaska-stock/tip-of-the-iceberg-digital-composite,aks0060246.jpg?max=128", image3 = "https://cdn2.iconfinder.com/data/icons/electric-devices/32/freezer_fridge_refrigerator_cold_kitchen_electrical_appliance_appliances_2-128.png"},
                new ModelClass { image1 = "https://www.shareicon.net/data/128x128/2015/10/29/663819_info_512x512.png", image2 = "http://www.i2clipart.com/cliparts/5/4/0/7/clipart-pi-5407.png", image3 = "https://68.media.tumblr.com/avatar_9a750741d272_128.png"},
                new ModelClass { image1 = "http://www.iconninja.com/files/281/844/995/dark-female-swimmer-icon.png", image2 = "https://www.shareicon.net/data/128x128/2016/02/22/723099_run_512x512.png", image3 = "http://www.iconninja.com/files/305/62/515/cyclist-helmet-icon.png"},
                new ModelClass { image1 = "https://cdn4.iconfinder.com/data/icons/cinema-line/64/seat_cinema_movie_theater_film-128.png", image2 = "https://images-na.ssl-images-amazon.com/images/I/81Cp3mnBOeL._CR412,0,1060,1060_UX128.jpg", image3 = "http://www.pixempire.com/images/preview/standing-man-laying-his-elbow-on-a-wall-icon.jpg"},
                new ModelClass { image1 = "https://www.shareicon.net/data/128x128/2016/04/12/748571_garden_512x512.png", image2 = "http://z6mag.com/wp-content/uploads/2011/12/lohan-playboy-cover.jpg", image3 = "http://photos1.blogger.com/blogger/4428/1500/200/I%20love%20Chastity%20copy.0.jpg" },
                new ModelClass { image1 = "https://68.media.tumblr.com/avatar_e190ba93e5e0_128.png", image2 = "https://68.media.tumblr.com/avatar_03210c5813e7_128.png", image3 = "http://cdn.quotesgram.com/img/17/79/1206776330-Bukhari_20Prophet_20Mohammed_128x128.png" }
            };

            return gameimages;
        }

        public static List<string> imageListHome()
        {
            List<string> homeimages = new List<string>();
            homeimages.Add("https://is5-ssl.mzstatic.com/image/thumb/Purple111/v4/c7/a4/07/c7a4077b-fa16-3f19-3ea2-87443233f24a/source/256x256bb.jpg");
            homeimages.Add("https://static-s.aa-cdn.net/img/gp/20600003790062/nB4yhpG_jVkmZvCStGLFqbowEY1ekdMm1iDAc2ZUadVaOQbdYwwXLlQF0-sMuTMk0g=w300?v=1");
            homeimages.Add("https://s-media-cache-ak0.pinimg.com/236x/ab/21/2f/ab212faa2fb1e56a08eb9984407d561b.jpg");
            homeimages.Add("https://pbs.twimg.com/profile_images/613340077155090432/un_i5k2Q.jpg");
            homeimages.Add("https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTqe-ILvxkMUFvI6GRK_At_9fGzzvlSwOyp58SRW40UT7wVEU0H");
            homeimages.Add("http://static.walashop.com/media/catalog/product/cache/2/small_image/256x/9df78eab33525d08d6e5fb8d27136e95/1/2/12111773-NAVY_BLAZER_1.jpg");
            homeimages.Add("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRd09DAWYONz9HdpXGXsPrWJIrhgKHGXwnsDSp9eC8I5HtSQVd4Kg");
            homeimages.Add("https://image.freepik.com/free-icon/maths_318-143211.jpg");
            homeimages.Add("https://www.resultsbase.net/images/eventtypes/ico_triathlon_256.png");
            homeimages.Add("https://images-na.ssl-images-amazon.com/images/I/71CUWobFnhL._CR204,0,1224,1224_UX128.jpg");
            homeimages.Add("http://photos1.blogger.com/blogger/4428/1500/200/R%20U%20Pure%20copy.0.jpg");
            homeimages.Add("https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcRczJLPX9BsEQdg3FlH6SA_bFXBrppt7GA2sGzQdUeE8uAVytlSIA");

            return homeimages;
        }
    }
}
