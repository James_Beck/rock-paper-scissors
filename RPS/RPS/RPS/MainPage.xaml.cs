﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RPS
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            Grid grid = new Grid { RowSpacing = 0, ColumnSpacing = 0 };
            int row = 0;
            int column = 0;
            int index = 0;

            for (int i = 0; i < 12; i++)
            {
                if (i == 0 || i == 11)
                {
                    grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                    grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                }
                else if (i + 1 % 3 == 0)
                {
                    grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                }
                else if (i + 1 % 4 == 0)
                {
                    grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                }
            }

            var tap = new TapGestureRecognizer();
            tap.Tapped += TapThat;

            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    var name = new Image { Source = Images.imageListHome()[index], BackgroundColor = Color.Black, Aspect = Aspect.Fill };
                    name.GestureRecognizers.Add(tap);
                    grid.Children.Add(name , column, row);
                    column++;
                    index++;
                }
                column = 0;
                row++;
            }

            Content = grid;
        }

        private void TapThat(object sender, EventArgs e)
        {
            int x = (int)((BindableObject)sender).GetValue(Grid.RowProperty) * 3;
            int y = (int)((BindableObject)sender).GetValue(Grid.ColumnProperty);
            int i = x + y;

            Navigation.PushAsync(new GamePage(Images.imagesListGames()[i].image1, Images.imagesListGames()[i].image2, Images.imagesListGames()[i].image3));
        }
    }
}
