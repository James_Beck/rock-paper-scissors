﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RPS
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GamePage : ContentPage
    {
        public GamePage(string img1, string img2, string img3)
        {
            InitializeComponent();

            Grid grid = new Grid { RowSpacing = 0, ColumnSpacing = 0 };
            int row = 0;
            int column = 0;

            for (int i = 0; i < 3; i++)
            {
                if (i == 0)
                {
                    grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                    grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                }
                else
                {
                    grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                }
            }

            var tap = new TapGestureRecognizer();
            tap.Tapped += TapThat;

            var value = "";
            var hex = "";

            for (int i = 0; i < 3; i++)
            {
                switch(i)
                {
                    case 0:
                        value = img1;
                        hex = "#F0F0F0";
                        break;

                    case 1:
                        value = img2;
                        hex = "#E8E8E8";
                        break;

                    case 2:
                        value = img3;
                        hex = "#E0E0E0";
                        break;
                }

                var name = new Image { Source = value, Aspect=Aspect.AspectFit, BackgroundColor = Color.FromHex(hex) };

                name.GestureRecognizers.Add(tap);
                grid.Children.Add(name, column, row);
                row++;
            }

            Content = grid;
        }

        private void TapThat (object sender, EventArgs e)
        {
            
        }
    }
}
